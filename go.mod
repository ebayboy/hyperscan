module gitee.com/ebayboy/hyperscan

go 1.17

require (
	github.com/google/gopacket v1.1.19
	github.com/smartystreets/goconvey v1.7.2
)

require (
	github.com/gopherjs/gopherjs v0.0.0-20181017120253-0766667cb4d1 // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/smartystreets/assertions v1.2.0 // indirect
	golang.org/x/sys v0.0.0-20190412213103-97732733099d // indirect
)
