package go_pcre

import (
	"testing"

	gopcre "github.com/gijsbers/go-pcre"
)

//全局变量
var re gopcre.Regexp

func BenchmarkMatch(t *testing.B) {
	for i := 0; i < t.N; i++ {
		Match(re)
	}
}

// 数据初始化，为了保证每次数据都是一致的
func TestMain(m *testing.M) {
	re = gopcre.MustCompile("a(x*)b(y|z)c", 0)

	m.Run()
}
