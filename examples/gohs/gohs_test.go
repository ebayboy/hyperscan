package main

import (
	"testing"
	"time"
)

//全局变量

func BenchmarkMatch(t *testing.B) {
	for i := 0; i < t.N; i++ {
		Match()
	}
}

// 数据初始化，为了保证每次数据都是一致的
func TestMain(m *testing.M) {

	InitMod()

	time.Sleep(time.Second * 3)

	m.Run()
}
